package main

import (
    "fmt"
    "log"
    "net/http"
    "os"
    "strconv"
)

func handler(w http.ResponseWriter, r *http.Request) {
        log.Print("Hello world received a request.")
        target := os.Getenv("TARGET")
        if target == "" {
                target = "World"
        }
        fmt.Fprintf(w, "Hello %s!\n", target)
}

func compl(n int64){
    if n>0 {
        compl(n-1)
        compl(n-1)
    }
}


func cpu(w http.ResponseWriter, req *http.Request) {

    for name, headers := range req.Header {
        for _, h := range headers {
            if name == "Register" {
                log.Println(h)
            }
            if name == "Complexity" {
                i, _ := strconv.ParseInt(h, 10, 64)
                compl(i)
            }
            //if(name == "memory"){
            //    i, _ := strconv.ParseInt("h", 10, 64)
            //    var a [i][i][i]int
            //    fmt.Fprintf(a[0][0][0])
            //}
            fmt.Fprintf(w, "%v: %v\n", name, h)
        }
    }
}



func main() {
    log.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds)
    log.Print("Hello world sample started.")

    http.HandleFunc("/", cpu)

    port := os.Getenv("PORT")
    if port == "" {
            port = "8080"
    }

    log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}

